#!/usr/bin/python3


import sys
import pprint
from subprocess import run

'''
# This is a multiline comment so I don't lose how to do the tracing stuff if I
# ever need to fine it again
def trace_calls(frame, event, arg):
    if event != 'call':
        return
    co = frame.f_code
    func_name = co.co_name
    if func_name == 'write':
        # Ignore write() calls from printing
        return
    func_line_no = frame.f_lineno
    func_filename = co.co_filename
    if not func_filename.endswith('sys_settrace_call.py'):
        # Ignore calls not in this module
        return
    caller = frame.f_back
    caller_line_no = caller.f_lineno
    caller_filename = caller.f_code.co_filename
    print('* Call to', func_name)
    print('*  on line {} of {}'.format(
        func_line_no, func_filename))
    print('*  from line {} of {}'.format(
        caller_line_no, caller_filename))
    return


sys.settrace(trace_calls)
'''

class OsInfo:
    def __init__(self, path):
        with open(path) as f:
            for line in f:
                param, value = line.split('=')
                setattr(self, param, value.strip())


os_info = OsInfo('/etc/os-release')

if os_info.ID == 'fedora':
    install_cmds = [
        ['dnf', 'clean', 'all'],
        ['dnf', 'update', '-y', 'fedora-repos'],
        ['dnf', 'update', '-y', 'fedora-gpg-keys', '--nogpgcheck'],
        ['dnf', 'install', '-y', 'python3-packaging'],
    ]

elif os_info.ID == 'ubuntu':
    install_cmds = [
        ['add-apt-repository', '-y', 'universe'],
        ['apt-get', 'update'],
        ['apt-get', 'install', '-y', 'python3-packaging']
    ]


for install_cmd in install_cmds:
    run(install_cmd)
